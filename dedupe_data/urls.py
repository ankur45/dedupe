from django.urls import path
from .views import cluster_ids

urlpatterns = [
    path('test', cluster_ids),
]
