from django.apps import AppConfig


class DedupeDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dedupe_data'
