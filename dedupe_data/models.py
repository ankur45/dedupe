from django.db import models

# Create your models here.
class DedupeInput(models.Model):
    policy_number=models.CharField(max_length=128, null=True)
    policy_holder_id=models.CharField(max_length=128, null=True)
    business_partner=models.CharField(max_length=128, null=True)
    dob=models.CharField(max_length=128, null=True)
    first_name=models.CharField(max_length=128, null=True)
    last_name=models.CharField(max_length=128, null=True)
    city=models.CharField(max_length=128, null=True)
    landmark=models.CharField(max_length=128, null=True)
    postal_code=models.CharField(max_length=128, null=True)
    policy_end_date=models.CharField(max_length=128, null=True)
    policy_current_status=models.CharField(max_length=128, null=True)


class DedupeOutput(models.Model):
    cluster_id=models.CharField(max_length=128, null=True)
    confidence_score=models.CharField(max_length=128, null=True)
    policy_number=models.CharField(max_length=128, null=True)
    policy_holder_id=models.CharField(max_length=128, null=True)
    business_partner=models.CharField(max_length=128, null=True)
    dob=models.CharField(max_length=128, null=True)
    first_name=models.CharField(max_length=128, null=True)
    last_name=models.CharField(max_length=128, null=True)
    city=models.CharField(max_length=128, null=True)
    landmark=models.CharField(max_length=128, null=True)
    postal_code=models.CharField(max_length=128, null=True)
    policy_end_date=models.CharField(max_length=128, null=True)
    policy_current_status=models.CharField(max_length=128, null=True)    